.. nims documentation master file, created by
   sphinx-quickstart on Thu Dec 22 11:42:41 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the NIMS documentation!
==================================

.. toctree::
   :maxdepth: 1
   
   Introduction <introduction>
   Build read-only webinterface <webinterface>
   Comment on systems architecture <architecture>
	

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
