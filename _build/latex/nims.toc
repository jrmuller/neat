\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Description}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Functional Requirement}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Technical Requirements}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Organization}{2}{section.1.5}
\contentsline {section}{\numberline {1.6}Resources}{2}{section.1.6}
\contentsline {section}{\numberline {1.7}Timing}{2}{section.1.7}
\contentsline {chapter}{\numberline {2}Build read-only NIMS webinterface}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Assignment}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Objective}{3}{section.2.3}
\contentsline {section}{\numberline {2.4}Functionalities}{4}{section.2.4}
\contentsline {chapter}{\numberline {3}Comment on NEAR architecture}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Tooling}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}Assignment}{5}{section.3.3}
\contentsline {section}{\numberline {3.4}Objective}{5}{section.3.4}
\contentsline {chapter}{\numberline {4}Indices and tables}{7}{chapter.4}
\contentsline {chapter}{Bibliography}{9}{chapter*.5}
