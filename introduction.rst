Introduction
============

:From: Jan-Ru Muller
:To: Ad-Jan Janmaat
:Subject: Web Application : NIMS Module
:Date: 16 December 2016
:Version: 2

Introduction
------------
Nostrum Oil & Gas is in the process of documenting its management information system (NIMS). Part of the NIMS is called NEAT (Nostrum Enterprise Activity Tracker). NEAT consists a database which is used to collect information on processes, responsibilities, tasks etc. There are presently approx. 10 users entering data (low volume). The database contains the information on about some 7 objects (People, Processes, Risks, Responsibilities, Software, ..) the information on which is captured in some 40 tables.

Description
-----------
The captured data shall be able to be re-used for different purposes. Also part of the data will be sourced from other systems (for example HR database). 1) For re-usability and for sourcing from other systems the data will be replicated in an (opensource) database. 2) Either the existing user interface is amended or a separate user interface is build taking into account the requirements of the project.

Functional Requirement
----------------------

•	Review the existing data model and suggest improvements
•	Define new views / reports
•	Review the existing UI and suggest improvements
•	Link to external data objects (documents, records)
•	Import from other data sources (ie HR data, Systems data)
•	Export to other database

Technical Requirements
----------------------
•	Linux based solution running on AWS (or SAP Hana Cloud)
•	Data replication / sharing from Microsoft SharePoint to open source database (i.e. PostgreSQL, mySQL)
•	JavaScript front end (using open source libraries i.e. Bootstrap, JQuery, SAPUI5)
•	Use of rapid development framework (i.e. Django or Flask-AppBuilder)
•	Use of version control system for code repository (GitHub)
•	Setting up of a continuous building / integration / deployment pipeline (Jenkins)
•	Application of robust testing (TDD)
•	Generate documentation from source code (Sphinx)
•	Consider multilingual GUI (u18i)
•	Modular approach (SOA)

Organization
------------
The developer will work for the project team of the so called ROME project based in Amsterdam (project manager George Gerards). The developer will also work with the IT department (functional head Jan-Ru Muller).

Resources
---------
Nostrum's IT department is in the process of developing skills with respect to front-end development using open source tooling. This capability will be build during the next coming year. If and when internal skills become available the developer may work in a team of two.

Timing
------
The project runs throughout 2017 (1 January – 31 December). The assignment is will be a total of 50 man-days on a part-time basis. 

