Comment on NEAR architecture
============================

Introduction
++++++++++++
NEAR [1]_ has been developed by the ROME team [NOG2016]_ to support capturing data about processes in the company.
Types of information captured are: "who performs a process", "what is the input", "where does the input stem from","what is the output", "where does the output go to", etc.
It is of critical importance that the data captured can be easily maintained and re-used. 

Tooling
+++++++
NEAT has been realized in MS SharePoint Online.
SharePoint Online has a flexible licensing structure.
At present 10 users have been defined for inputing data into the system.

The backend of the MS SharePoint Online 'Portal' is an MS SQL database.
The MS SQL database is accessible via MS Excel, via MS Access, via ODBC etc.

Care has been taken to use reference lists were possible to limit "free form" user input.
Macros (Stored Procedures) are used to maintain database consistency and integrity.

Assignment
++++++++++
Review and comment on:

•	The datamodel  (datatyping, tablestructure) - to be provided
•	The user interface (already provided)
•	The logic (macros, data macros, input validation rules) - to be provided
•	Reporting possibilities 
•	Possibility to use SOA or REST based architecture

Objective
+++++++++
On the basis of the architecture of NEAR the ROME team can determine how to further develop the application. 
Data will be captured where it originates and it will be maintained by the data owner. Data duplication is avoided.
Required functionality will be addressed by the ROME team and is added in modular fashion.
The prototyping approach is maintained while parts (modules) of the system are (re)written for production use and handover to ICT.


.. [1] NEAR = Nostrum Enterprise Architecture Tracker

.. [NOG2016] Brussels, November 2016 
   Functional organization and Integrated Management System

.. Comments begin with two dots and a space.
	Datamodel
	---------
	Objects
	-------
	There are some 7 objects defined in NEAR. 	
	Systems
	-------


   