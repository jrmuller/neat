Build read-only NIMS webinterface
=================================

Introduction
++++++++++++

NIMS [1]_ generally refers to the grand system, documented or undocumented, consisting of manual processes or automated processes. When we refer to NIMS in this document it has a more narrow meaning, i.e. the "system supporting the ROME project to create a enterprise model, the basis of future enterprise applications".

The very nature of NIMS is that it integrates various business functions and, let's call it, "information domains". Naturally NIMS will thus span multiple systems and "data silos"/data sources. Examples of such source systems are:

* an employee database;

* a (software) systems database.

NIMS is planned to extracts information from different "source systems". Most users are "read-only users" who will query NIMS for various reports.

Assignment
++++++++++

Build a webfrontend to (at least) two different datasources allowing for easy navigation and data interaction.
The specific use case is to display the use of systems by different users / functions. Users and functions are stored in SQL Server. For the purpose of this assignment systems are stored in MySQL. Both systems have there own maintenance (CRUD) forms/routines.

The to be build webfrontend will allow for easy navigation of a subset of the data from both systems. Reporting will be flexible but for example it can be shown:

* which applications used by which user (application / user);
* which applications are used by which function (application / function)

Objective
+++++++++

The objective of building the "read-only webinterface" is to demonstrate that in one screen the user can be presented information stemming from 2 underlying databases.

How does this support the ROME project?
The objective of this assignment for the ROME project is to show the feasability of having a webfrontend to multiple datasources.

Ultimately the ROME project will result in refactoring of existing systems. It thus will be helpfull to be able to incorporate existing data from existing system. 	

Functionalities
+++++++++++++++
* Ease of use
* Integration
* Multi-User
* No duplication of data
* Web interface
* Flexible reporting (screen/print)
* Multi language (RU/EN)

.. [1] NIMS = Nostrum Integrated Management System